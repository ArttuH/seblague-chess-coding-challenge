﻿//#define LOG
#define NOLOG

using ChessChallenge.API;
//using Microsoft.CodeAnalysis.CSharp;
using System;
using System.Diagnostics;
//using System.ComponentModel;

public class AlaskaBot : IChessBot
{
    const float EVALUATION_PAWN = 1f;
    const float EVALUATION_KNIGHT = 3f;
    const float EVALUATION_BISHOP = 3f;
    const float EVALUATION_ROOK = 5f;
    const float EVALUATION_QUEEN = 9f;
    const float EVALUATION_CHECK = 2f;//100f;
    const float EVALUATION_MATE = 9001f;
    const float EVALUATION_DRAW = -1000f;
    const float EVALUATION_USELESS_MOVE = -1f;

    bool playingAsWhite = false;

    public Move Think(Board board, Timer timer)
    {
        playingAsWhite = board.IsWhiteToMove;

        //Console.WriteLine($"Bot playing as {(playingAsWhite ? "white" : "black")}");

        Move[] moves = board.GetLegalMoves();

        //int depth = timer.MillisecondsRemaining > 1e4 ? 4 : 4;

        Move bestMove = FindBestMove(board, moves, true, 4); // 4

        //Console.WriteLine($"***\nPlaying move {MoveToString(bestMove)}\n***\n");

        return bestMove;
    }

    Move FindBestMove(Board board, Move[] legalMoves, bool ourMove, int depth)
    {
#if LOG
        Console.WriteLine($"\nDepth: {depth}\n");
#endif

        float evaluationBeforeMove = Evaluate(board);

        float[] evaluations = new float[legalMoves.Length];

        for (int i = 0; i < legalMoves.Length; ++i)
        {
            Move move = legalMoves[i];

            int depthForThisLine = depth;

            if (depthForThisLine > 1)
            {
#if LOG
                Console.WriteLine($"\n\nLooking at {(ourMove ? "our" : "opponent's")} move {MoveToString(move)}");
#endif
                evaluations[i] = Evaluate(move, board);

                if (!EvaluationIsBetter(evaluations[i], evaluationBeforeMove, board.IsWhiteToMove/*ourMove && playingAsWhite*/))
                {
                    evaluations[i] += (playingAsWhite ? 1 : -1) * EVALUATION_USELESS_MOVE; // Penalize the useless move
#if LOG
                    Console.WriteLine($"No need to evaluate any further, as this move does not improve the board state");
#endif
                    //continue;

                    // Only look one move ahead to see if the "useless" move was actually to dodge a capture
                    if (depthForThisLine > 2)
                        depthForThisLine = 2;
                }
#if LOG
                Console.WriteLine($"By itself the move leads to evaluation {evaluations[i]} for a change of {evaluations[i] - evaluationBeforeMove}");
#endif

                board.MakeMove(move);

                if (board.IsInCheckmate())
                {
                    board.UndoMove(move);
                    return move;
                }
                else if (board.IsDraw())
                {
                    evaluations[i] = Evaluate(board);
                    board.UndoMove(move);
                }
                else
                {
                    Move bestCounterMove = FindBestMove(board, board.GetLegalMoves(), !ourMove, depthForThisLine - 1);
                    board.MakeMove(bestCounterMove);
                    evaluations[i] = Evaluate(board);

#if LOG
                    Console.WriteLine($"At depth {depth}, against {MoveToString(move)}, the best counter move for {(ourMove ? "opponent" : "us")} is {MoveToString(bestCounterMove)}, leading to evaluation {evaluations[i]}\n");
#endif
                    board.UndoMove(bestCounterMove);
                    board.UndoMove(move);
                }
            }
            else
            {
#if LOG
                if (ourMove)
                    Console.WriteLine($"\n\nLooking at {(ourMove ? "our" : "opponent's")} move {MoveToString(move)}");
#endif
                evaluations[i] = Evaluate(move, board);
            }
        }

        bool who = ourMove ? playingAsWhite : !playingAsWhite;

        int bestMoveIndex = FindBestMoveIndex(evaluations, who);

        Move bestMove = legalMoves[bestMoveIndex];

#if LOG
        Console.WriteLine($"\nBest move for {(ourMove ? "us" : "opponent")} at depth {depth} is {MoveToString(bestMove)}");
        Console.WriteLine($"\nThis leads to evaluation {evaluations[bestMoveIndex]} for a change of {evaluations[bestMoveIndex] - evaluationBeforeMove}");
#endif

        return bestMove;
    }

    float Evaluate(Move move, Board board)
    {
        //Console.WriteLine($"Looking at move {MoveToString(move)}");

        board.MakeMove(move);
        float evaluation = Evaluate(board);

        board.UndoMove(move);

        return evaluation;
    }

    float Evaluate(Board board)
    {
        //Console.WriteLine($"board.IsWhiteToMove == {board.IsWhiteToMove}");

        Stopwatch sw = new Stopwatch();
        sw.Start();

        float evaluation = 0f;

        if (board.IsInCheckmate())
            evaluation += EVALUATION_MATE * (board.IsWhiteToMove ? -1 : 1);
        else if (board.IsInCheck())
            evaluation += EVALUATION_CHECK * (board.IsWhiteToMove ? -1 : 1);
        else if (board.IsDraw())
            evaluation += EVALUATION_DRAW * (board.IsWhiteToMove ? -1 : 1);

        PieceList whitePawns = board.GetPieceList(PieceType.Pawn, true);
        PieceList whiteKnights = board.GetPieceList(PieceType.Knight, true);
        PieceList whiteBishops = board.GetPieceList(PieceType.Bishop, true);
        PieceList whiteRooks = board.GetPieceList(PieceType.Rook, true);
        PieceList whiteQueens = board.GetPieceList(PieceType.Queen, true);
        PieceList blackPawns = board.GetPieceList(PieceType.Pawn, false);
        PieceList blackKnights = board.GetPieceList(PieceType.Knight, false);
        PieceList blackBishops = board.GetPieceList(PieceType.Bishop, false);
        PieceList blackRooks = board.GetPieceList(PieceType.Rook, false);
        PieceList blackQueens = board.GetPieceList(PieceType.Queen, false);

        foreach (Piece piece in whitePawns)
            evaluation += EVALUATION_PAWN + EvaluatePawnSquare(piece.Square, true);
        foreach (Piece piece in whiteKnights)
            evaluation += EVALUATION_KNIGHT + EvaluateKnightSquare(piece.Square, true);
        foreach (Piece piece in whiteBishops)
            evaluation += EVALUATION_BISHOP;
        foreach (Piece piece in whiteRooks)
            evaluation += EVALUATION_ROOK;
        foreach (Piece piece in whiteQueens)
            evaluation += EVALUATION_QUEEN;

        // King safety?

        foreach (Piece piece in blackPawns)
            evaluation += -EVALUATION_PAWN + EvaluatePawnSquare(piece.Square, false);
        foreach (Piece piece in blackKnights)
            evaluation += -EVALUATION_KNIGHT + EvaluateKnightSquare(piece.Square, false);
        foreach (Piece piece in blackBishops)
            evaluation += -EVALUATION_BISHOP;
        foreach (Piece piece in blackRooks)
            evaluation += -EVALUATION_ROOK;
        foreach (Piece piece in blackQueens)
            evaluation += -EVALUATION_QUEEN;

        // King safety?

        //Console.WriteLine($"Evaluation: {evaluation}");

        sw.Stop();
        long microseconds = sw.ElapsedTicks / (Stopwatch.Frequency / (1000L * 1000L));

        return evaluation;
    }

    int FindBestMoveIndex(float[] evaluations, bool playingAsWhite)
    {
        float largestPositiveEvaluation = evaluations[0];
        float largestNegativeEvaluation = evaluations[0];

        int largestPositiveEvaluationIndex = 0;
        int largestNegativeEvaluationIndex = 0;

        for (int i = 0; i < evaluations.Length; ++i)
        {
            if (evaluations[i] > largestPositiveEvaluation)
            {
                largestPositiveEvaluation = evaluations[i];
                largestPositiveEvaluationIndex = i;
            }

            if (evaluations[i] < largestNegativeEvaluation)
            {
                largestNegativeEvaluation = evaluations[i];
                largestNegativeEvaluationIndex = i;
            }
        }

        if (playingAsWhite)
            return largestPositiveEvaluationIndex;
        else
            return largestNegativeEvaluationIndex;
    }

    float EvaluatePawnSquare(Square square, bool white)
    {
        int relativeRank = white ? (square.Rank - 1) : (6 - square.Rank);

        float distanceFromCenter = square.File <= 3 ? (3.5f - square.File) : (square.File - 3.5f);

        float evaluation = (relativeRank) * (white ? 1 : -1) * 0.1f;

        evaluation = evaluation / distanceFromCenter;

        if (white == false)
        {
            int a = 0;
        }

        return evaluation;
    }

    float EvaluateKnightSquare(Square square, bool white)
    {
        float rankDistanceFromCenter = square.Rank <= 3 ? (3.5f - square.Rank) : (square.Rank - 3.5f);
        float fileDistanceFromCenter = square.File <= 3 ? (3.5f - square.File) : (square.File - 3.5f);
        float evaluation = (white ? 1 : -1) / (rankDistanceFromCenter + fileDistanceFromCenter);
        return evaluation;
    }

    string MoveToString(Move move)
    {
        return $"{move.MovePieceType} from {move.StartSquare.Name} to {move.TargetSquare.Name}";
    }

    bool EvaluationIsBetter(float evaluation, float baselineEvaluation, bool playingAsWhite)
    {
        if (playingAsWhite)
            return evaluation > baselineEvaluation;
        else
            return evaluation < baselineEvaluation;
    }
}