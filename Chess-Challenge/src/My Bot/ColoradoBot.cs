﻿//#define LOG
#define NOLOG

using ChessChallenge.API;
using System;

public class ColoradoBot : IChessBot
{
    const float EVALUATION_PAWN = 1f;
    const float EVALUATION_KNIGHT = 3f;
    const float EVALUATION_BISHOP = 3f;
    const float EVALUATION_ROOK = 5f;
    const float EVALUATION_QUEEN = 9f;
    const float EVALUATION_CHECK = 2f;//100f;
    const float EVALUATION_MATE = 9001f;
    const float EVALUATION_DRAW = -1000f;
    const float EVALUATION_USELESS_MOVE = -1f;

    bool playingAsWhite = false;

    //int previousDepth = 0;

    //enum Phase
    //{
    //    Opening,
    //    MiddleGame,
    //    EndGame
    //}

    //Phase phase;

    //float[,,] evaluationLookUp;
    float[,] evaluationLookUp;

#if LOG
    int evaluateCounter;
    int deltaEvaluateCounter;
    float currentEvaluation;
#endif

    public ColoradoBot()
    {
        //phase = Phase.Opening;

        evaluationLookUp = new float[7, 8 * 8]; // [0][x*y] is dummy!

        CreateEvaluationLookupTable();

#if LOG
        evaluateCounter = 0;
        deltaEvaluateCounter = 0;
        currentEvaluation = 0f;
#endif
    }

    public Move Think(Board board, Timer timer)
    {
        //        Move[] moveHistory = board.GameMoveHistory;

        //        if (moveHistory.Length > 0)
        //        {
        //            Move previousMove = moveHistory[moveHistory.Length - 1];
        //#if LOG
        //            currentEvaluation += DeltaEvaluation(previousMove, !board.IsWhiteToMove);
        //#endif
        //        }

        playingAsWhite = board.IsWhiteToMove;
        //float evaluationBeforeMove = Evaluate(board);

        Move bestMove = FindBestMove(board/*, true*/, 8); // 6 // 4 // 9        
        //Move bestMove = FindBestMove(board, true, 5); // 4 // 9

        return bestMove;
    }

    Move FindBestMove(Board board/*, bool ourMove*/, int depth)
    {
        Move[] legalMoves = board.GetLegalMoves();

        float[] evaluations = new float[legalMoves.Length];

        for (int i = 0; i < legalMoves.Length; i++)
        {
            int depthForThisLine = depth;

            Move moveToEvaluate = legalMoves[i];

            float delta = DeltaEvaluation(moveToEvaluate, board.IsWhiteToMove);
            evaluations[i] += delta;

            bool moveLooksBad = (delta < 0) == board.IsWhiteToMove;
            if (depthForThisLine > 2 && moveLooksBad)
                depthForThisLine = 2;

            if (depthForThisLine > 1)
            {
                board.MakeMove(moveToEvaluate);

                if (board.IsInCheckmate())
                {
                    board.UndoMove(moveToEvaluate);
                    return moveToEvaluate;
                    //evaluations[i] += EVALUATION_MATE * (board.IsWhiteToMove ? 1 : -1);
                }
                else if (board.IsDraw())
                {
                    evaluations[i] += EVALUATION_DRAW * (board.IsWhiteToMove ? 1 : -1);
                }
                else
                {
                    //Move bestCounterMove = FindBestMove(board/*, !ourMove*/, depth - 1);
                    //evaluations[i] += DeltaEvaluation(bestCounterMove, board.IsWhiteToMove);
                    evaluations[i] += FindBestMoveEvaluationBreadthFirst(board, depthForThisLine - 1);
                }

                board.UndoMove(moveToEvaluate);
            }
        }

        Move bestMove = FindBestMove(evaluations, legalMoves, board.IsWhiteToMove);

        return bestMove;
    }

    float FindBestMoveEvaluation(Board board, int depth)
    {
        //if (depth != previousDepth)
        //{
        //    Console.WriteLine($"FindBestMoveEvaluation at depth {depth}");
        //    previousDepth = depth;
        //}

        Move[] legalMoves = board.GetLegalMoves();

        float[] evaluations = new float[legalMoves.Length];

        for (int i = 0; i < legalMoves.Length; i++)
        {
            int depthForThisLine = depth;

            Move moveToEvaluate = legalMoves[i];

            //evaluations[i] += DeltaEvaluation(moveToEvaluate, board.IsWhiteToMove);
            float delta = DeltaEvaluation(moveToEvaluate, board.IsWhiteToMove);
            evaluations[i] += delta;

            bool moveLooksBad = (delta < 0) == board.IsWhiteToMove;
            //if (depthForThisLine > 2 && moveLooksBad)
            //    depthForThisLine = 2;
            if (depthForThisLine > 1 && moveLooksBad)
                depthForThisLine = 1;

            if (depthForThisLine > 1)
            {
                board.MakeMove(moveToEvaluate);

                if (board.IsInCheckmate())
                {
                    board.UndoMove(moveToEvaluate);
                    return EVALUATION_MATE * (board.IsWhiteToMove ? 1 : -1);
                    //evaluations[i] += EVALUATION_MATE * (board.IsWhiteToMove ? 1 : -1);
                }
                else if (board.IsDraw())
                {
                    evaluations[i] += EVALUATION_DRAW * (board.IsWhiteToMove ? 1 : -1);
                }
                else
                {
                    //Move bestCounterMove = FindBestMove(board/*, !ourMove*/, depth - 1);
                    //evaluations[i] += DeltaEvaluation(bestCounterMove, board.IsWhiteToMove);
                    evaluations[i] += FindBestMoveEvaluation(board, depthForThisLine - 1);
                }

                board.UndoMove(moveToEvaluate);
            }
        }

        //Move bestMove = FindBestMove(evaluations, legalMoves, board.IsWhiteToMove);
        float bestEvaluation = FindBestMoveEvaluation(evaluations, legalMoves, board.IsWhiteToMove);

        return bestEvaluation;
    }

    float FindBestMoveEvaluationBreadthFirst(Board board, int depth)
    {
        //if (depth != previousDepth)
        //{
        //    Console.WriteLine($"FindBestMoveEvaluation at depth {depth}");
        //    previousDepth = depth;
        //}

        Move[] legalMoves = board.GetLegalMoves();

        float[] evaluations = new float[legalMoves.Length];

        for (int i = 0; i < legalMoves.Length; i++)
        {
            Move moveToEvaluate = legalMoves[i];

            float delta = DeltaEvaluation(moveToEvaluate, board.IsWhiteToMove);
            evaluations[i] += delta;
        }

        Array.Sort(evaluations, legalMoves);
        if (!board.IsWhiteToMove)
        {
            Array.Reverse(evaluations);
            Array.Reverse(legalMoves);
        }

        int divisor = depth > 1 ? (depth + 2) : 1;
        //int threshold = legalMoves.Length / depth;
        int threshold = legalMoves.Length / divisor;

        //Console.WriteLine($"Length = {legalMoves.Length} and Depth = {depth} -> threshold = {threshold}");

        //for (int i = 0; i < legalMoves.Length; i++)
        for (int i = 0; i < threshold; i++)
        {
            int depthForThisLine = depth;
            Move moveToEvaluate = legalMoves[i];

            if (depthForThisLine > 1)
            {
                board.MakeMove(moveToEvaluate);

                if (board.IsInCheckmate())
                {
                    board.UndoMove(moveToEvaluate);
                    return EVALUATION_MATE * (board.IsWhiteToMove ? 1 : -1);
                    //evaluations[i] += EVALUATION_MATE * (board.IsWhiteToMove ? 1 : -1);
                }
                else if (board.IsDraw())
                {
                    evaluations[i] += EVALUATION_DRAW * (board.IsWhiteToMove ? 1 : -1);
                }
                else
                {
                    //Move bestCounterMove = FindBestMove(board/*, !ourMove*/, depth - 1);
                    //evaluations[i] += DeltaEvaluation(bestCounterMove, board.IsWhiteToMove);
                    evaluations[i] += FindBestMoveEvaluationBreadthFirst(board, depthForThisLine - 1);
                }

                board.UndoMove(moveToEvaluate);
            }
        }

        //Move bestMove = FindBestMove(evaluations, legalMoves, board.IsWhiteToMove);
        float bestEvaluation = FindBestMoveEvaluation(evaluations, legalMoves, board.IsWhiteToMove);

        return bestEvaluation;
    }

    /*Move FindBestMove(Board board, bool ourMove, int depth)
    {
        Move[] legalMoves = board.GetLegalMoves(); 

        float[] evaluations = new float[legalMoves.Length];

        for (int i = 0; i < legalMoves.Length; ++i)
        {
            float evaluationForThisLine = 0;

            Move move = legalMoves[i];

            float deltaForThisMove = DeltaEvaluation(move, board.IsWhiteToMove);

            int depthForThisLine = depth;

            if (depthForThisLine > 1)
            {
#if LOG
                Console.WriteLine($"\n\nLooking at {(ourMove ? "our" : "opponent's")} move {MoveToString(move)}");
#endif
                //evaluations[i] = Evaluate(move, board);

                //if (!EvaluationIsBetter(evaluations[i], evaluationBeforeMove, board.IsWhiteToMove))
                if ((board.IsWhiteToMove && deltaForThisMove <= 0) || (!board.IsWhiteToMove && deltaForThisMove >= 0))
                {
                    evaluationForThisLine += (board.IsWhiteToMove ? 1 : -1) * EVALUATION_USELESS_MOVE; // Penalize the useless move
#if LOG
                    Console.WriteLine($"No need to evaluate any further, as this move does not improve the board state");
#endif
                    // Only look one move ahead to see if the "useless" move was actually to dodge a capture
                    if (depthForThisLine > 2)
                        depthForThisLine = 2;
                }
#if LOG
                //Console.WriteLine($"By itself the move leads to evaluation {evaluations[i]} for a change of {evaluations[i] - evaluationBeforeMove}");
#endif

                evaluationForThisLine += DeltaEvaluation(move, board.IsWhiteToMove);
                board.MakeMove(move);

                if (board.IsInCheckmate())
                {
                    board.UndoMove(move);
                    return move;
                }
                else if (board.IsDraw())
                {
                    //evaluations[i] = Evaluate(board);
                    evaluationForThisLine += (board.IsWhiteToMove ? -1 : 1) * EVALUATION_DRAW;
                    board.UndoMove(move);
                }
                else
                {
                    Move bestCounterMove = FindBestMove(board, !ourMove, depthForThisLine - 1);
                    evaluationForThisLine += DeltaEvaluation(bestCounterMove, board.IsWhiteToMove);
                    //board.MakeMove(bestCounterMove);
                    //evaluations[i] = Evaluate(board);
#if LOG
                    Console.WriteLine($"At depth {depth}, against {MoveToString(move)}, the best counter move for {(ourMove ? "opponent" : "us")} is {MoveToString(bestCounterMove)}, leading to evaluation {evaluations[i]}\n");
#endif
                    //board.UndoMove(bestCounterMove);
                    //Console.WriteLine($"Best countermove for {(board.IsWhiteToMove ? "white" : "black")} {MoveToString(bestCounterMove)} changes evaluation from {sanityCheck} to {evaluations[i]}");
                    board.UndoMove(move);
                }

                evaluations[i] = evaluationForThisLine + deltaForThisMove;
            }
            else
            {
#if LOG
                if (ourMove)
                    Console.WriteLine($"\n\nLooking at {(ourMove ? "our" : "opponent's")} move {MoveToString(move)}");
#endif
                //evaluations[i] = Evaluate(move, board);
                evaluations[i] = deltaForThisMove;
            }

            //Console.WriteLine($"Evaluation after {(board.IsWhiteToMove ? "white" : "black")}'s move {MoveToString(move)} was updated from {evaluationByItself} to {evaluations[i]}");
        }

        Move bestMove = FindBestMove(evaluations, legalMoves, board.IsWhiteToMove);

        return bestMove;
    }*/

    /*float Evaluate(Move move, Board board)
    {
#if LOG
        //Console.WriteLine($"Looking at move {MoveToString(move)}");
#endif

        float evaluation = DeltaEvaluation(move, board.IsWhiteToMove);

        board.MakeMove(move);
        evaluation += Evaluate(board);
        board.UndoMove(move);

        return evaluation;
    }*/

    float Evaluate(Board board/*, bool updatePhase = false*/)
    {
#if LOG
        ++evaluateCounter;
        //Console.WriteLine($"board.IsWhiteToMove == {board.IsWhiteToMove}");
#endif

        float evaluation = 0f;

        //int totalPieces = 0;
        //int ownUndevelopedPieces = 0;

        if (board.IsInCheckmate())
            evaluation += EVALUATION_MATE;
        else if (board.IsInCheck())
            evaluation += EVALUATION_CHECK;
        else if (board.IsDraw())
            evaluation += EVALUATION_DRAW;

        evaluation *= (board.IsWhiteToMove ? -1 : 1);

        //evaluation += EvaluatePieces(board, true/*, ref totalPieces, ref ownUndevelopedPieces*/);
        //evaluation += EvaluatePieces(board, false/*, ref totalPieces, ref ownUndevelopedPieces*/);

        //        if (updatePhase && phase == Phase.Opening && ownUndevelopedPieces <= 1)
        //        {
        //            phase = Phase.MiddleGame;
        //#if LOG
        //#endif
        //            Console.WriteLine("Transitioning to middlegame!");
        //        }
        //        if (updatePhase && phase != Phase.EndGame && totalPieces < 6)
        //        {
        //            phase = Phase.EndGame;
        //#if LOG
        //#endif
        //            Console.WriteLine("We're in the endgame now!");
        //        }

        //Console.WriteLine($"Evaluation: {evaluation}");

        return evaluation;
    }

    /*float EvaluatePieces(Board board, bool evaluateForWhite/*, ref int totalPieces, ref int ownUndevelopedPieces* /)
    {
        //Stopwatch sw = new Stopwatch();
        //Stopwatch sw1 = new Stopwatch();
        //Stopwatch sw2 = new Stopwatch();
        //sw.Start();
        
        //sw1.Start();
        float evaluation = 0;

        PieceList pawns = board.GetPieceList(PieceType.Pawn, evaluateForWhite);
        PieceList knights = board.GetPieceList(PieceType.Knight, evaluateForWhite);
        PieceList bishops = board.GetPieceList(PieceType.Bishop, evaluateForWhite);
        PieceList rooks = board.GetPieceList(PieceType.Rook, evaluateForWhite);
        PieceList queens = board.GetPieceList(PieceType.Queen, evaluateForWhite);
        PieceList king = board.GetPieceList(PieceType.King, evaluateForWhite);

        //sw1.Stop();
        //sw2.Start();

        foreach (Piece piece in pawns)
            evaluation += evaluationLookUp[(int)piece.PieceType, piece.Square.File, piece.Square.Rank];
        foreach (Piece piece in knights)
            evaluation += evaluationLookUp[(int)piece.PieceType, piece.Square.File, piece.Square.Rank];
        foreach (Piece piece in bishops)
            evaluation += evaluationLookUp[(int)piece.PieceType, piece.Square.File, piece.Square.Rank];
        foreach (Piece piece in rooks)
            evaluation += evaluationLookUp[(int)piece.PieceType, piece.Square.File, piece.Square.Rank];
        foreach (Piece piece in queens)
            evaluation += evaluationLookUp[(int)piece.PieceType, piece.Square.File, piece.Square.Rank];

        evaluation += evaluationLookUp[(int)king[0].PieceType, king[0].Square.File, king[0].Square.Rank];

        evaluation *= (evaluateForWhite ? 1 : -1);

        //sw2.Stop();
        //sw.Stop();

        //long microseconds = sw.ElapsedTicks / (Stopwatch.Frequency / (1000L * 1000L));
        //long microseconds1 = sw1.ElapsedTicks / (Stopwatch.Frequency / (1000L * 1000L));
        //long microseconds2 = sw2.ElapsedTicks / (Stopwatch.Frequency / (1000L * 1000L));
        //long nanoseconds = sw.ElapsedTicks / (Stopwatch.Frequency / (1000L * 1000L * 1000L));

        return evaluation;
    }*/

    Move FindBestMove(float[] evaluations, Move[] legalMoves, bool evaluateForWhite)
    {
        //Array.Sort(evaluations, legalMoves);

        //if (evaluateForWhite)
        //    return legalMoves[legalMoves.Length - 1];
        //else
        //    return legalMoves[0];

        float bestEvaluation = evaluations[0];
        int bestMoveIndex = 0;

        for (int i = 1; i < legalMoves.Length; ++i)
        {
            float evaluation = evaluations[i];
            //if (evaluateForWhite)
            //{
            if (evaluateForWhite && (evaluation > bestEvaluation) ||
                !evaluateForWhite && (evaluation < bestEvaluation))
            {
                bestEvaluation = evaluation;
                bestMoveIndex = i;
            }
            //}
        }

        return legalMoves[bestMoveIndex];
    }

    float FindBestMoveEvaluation(float[] evaluations, Move[] legalMoves, bool evaluateForWhite)
    {
        //Array.Sort(evaluations, legalMoves);

        //if (evaluateForWhite)
        //    return evaluations[legalMoves.Length - 1];
        //else
        //    return evaluations[0];

        float bestEvaluation = evaluations[0];

        for (int i = 1; i < legalMoves.Length; ++i)
        {
            float evaluation = evaluations[i];
            //if (evaluateForWhite)
            //{
            if (evaluateForWhite && (evaluation > bestEvaluation) ||
                !evaluateForWhite && (evaluation < bestEvaluation))
            {
                bestEvaluation = evaluation;
            }
            //}
        }

        return bestEvaluation;
    }

    float EvaluatePawnSquare(int file, int rank)
    {
        float distanceFromCenter = file <= 3 ? (3.5f - file) : (file - 3.5f);

        float evaluation = rank * 0.125f;

        evaluation /= distanceFromCenter;

        return evaluation;
    }

    float EvaluateKnightSquare(int file, int rank)
    {
        float fileDistanceFromCenter = file <= 3 ? (3.5f - file) : (file - 3.5f);

        float rankDistanceFromCenter = rank <= 3 ? (3.5f - rank) : (rank - 3.5f);

        float evaluation = 1 / (rankDistanceFromCenter + fileDistanceFromCenter);

        //float evaluation;
        //if (phase == Phase.Opening)
        //{
        //    evaluation = rank <= 1 ? 0.0f : rank < 4 ? 0.6f : 0.0f;
        //    evaluation /= fileDistanceFromCenter;
        //}
        //else
        //{
        //    float rankDistanceFromCenter = rank <= 3 ? (3.5f - rank) : (rank - 3.5f);

        //    evaluation = 1 / (rankDistanceFromCenter + fileDistanceFromCenter);
        //}

        return evaluation;
    }

    float EvaluateBishopSquare(int file, int rank)
    {
        //float evaluation = 0;
        //if (phase == Phase.Opening)
        //    evaluation = relativeRank == 0 ? 0.0f : relativeRank < 4 ? 0.35f : 0;

        float evaluation = rank == 0 ? 0.0f : rank < 4 ? 0.35f : 0;

        return evaluation;
    }

    float EvaluateRookSquare(int file, int rank)
    {
        float evaluation = file == 0 ? 0.0f : file == 7 ? 0.0f : 0.1f;

        //if (phase == Phase.Opening)
        //    evaluation = square.File == 0 ? 0.0f : square.File == 7 ? 0.0f : 0.1f;

        return evaluation;
    }

    float EvaluateQueenSquare(int file, int rank)
    {
        float evaluation = 0;

        return evaluation;
    }

    float EvaluateKingSquare(int file, int rank)
    {
        float fileDistanceFromSafety = file <= 3 ? MathF.Abs(1.1f - file) : MathF.Abs(5.9f - file);
        float evaluation = 0.5f / (fileDistanceFromSafety + rank);

        //int relativeRank = white ? (square.Rank) : (7 - square.Rank);

        //if (phase == Phase.MiddleGame)
        //{
        //    float fileDistanceFromSafety = ownSquare.File <= 3 ? MathF.Abs(1.1f - ownSquare.File) : MathF.Abs(5.9f - ownSquare.File);
        //    evaluation = 0.5f / (fileDistanceFromSafety + (white ? (ownSquare.Rank) : (7 - ownSquare.Rank)));
        //}
        //else if (phase == Phase.EndGame)
        //{
        //    float distanceSquaredFromOpponentKing = (ownSquare.File - opponentSquare.File) * (ownSquare.File - opponentSquare.File) +
        //        (ownSquare.Rank - opponentSquare.Rank) * (ownSquare.Rank - opponentSquare.Rank);

        //    evaluation = 1 / distanceSquaredFromOpponentKing;
        //}

        return evaluation;
    }

#if LOG
    string MoveToString(Move move)
    {
        return $"{move.MovePieceType} from {move.StartSquare.Name} to {move.TargetSquare.Name}";
    }
#endif

    /*
    bool EvaluationIsBetter(float evaluation, float baselineEvaluation, bool evaluateForWhite)
    {
        return evaluateForWhite ? evaluation > baselineEvaluation : evaluation < baselineEvaluation;
    }
    */

    void CreateEvaluationLookupTable()
    {
        // None = 0, Pawn = 1, Knight = 2, Bishop = 3, Rook = 4, Queen = 5, King = 6

        // Pawn
        for (int file = 0; file < 8; ++file)
            for (int rank = 0; rank < 8; ++rank)
                evaluationLookUp[(int)PieceType.Pawn, file + (8 * rank)] = EVALUATION_PAWN + EvaluatePawnSquare(file, rank);

        // Knight
        for (int file = 0; file < 8; ++file)
            for (int rank = 0; rank < 8; ++rank)
                evaluationLookUp[(int)PieceType.Knight, file + (8 * rank)] = EVALUATION_KNIGHT + EvaluateKnightSquare(file, rank);

        // Bishop
        for (int file = 0; file < 8; ++file)
            for (int rank = 0; rank < 8; ++rank)
                evaluationLookUp[(int)PieceType.Bishop, file + (8 * rank)] = EVALUATION_BISHOP + EvaluateBishopSquare(file, rank);

        // Rook
        for (int file = 0; file < 8; ++file)
            for (int rank = 0; rank < 8; ++rank)
                evaluationLookUp[(int)PieceType.Rook, file + (8 * rank)] = EVALUATION_ROOK + EvaluateRookSquare(file, rank);

        // Queen
        for (int file = 0; file < 8; ++file)
            for (int rank = 0; rank < 8; ++rank)
                evaluationLookUp[(int)PieceType.Queen, file + (8 * rank)] = EVALUATION_QUEEN + EvaluateQueenSquare(file, rank);

        // King
        for (int file = 0; file < 8; ++file)
            for (int rank = 0; rank < 8; ++rank)
                evaluationLookUp[(int)PieceType.King, file + (8 * rank)] = EvaluateKingSquare(file, rank);
    }

    float DeltaEvaluation(Move move, bool evaluateForWhite)
    {
#if LOG
        ++deltaEvaluateCounter;
#endif

        float delta = 0;

        int relativeTargetSquareRank = evaluateForWhite ? move.TargetSquare.Rank : 7 - move.TargetSquare.Rank;

        delta -= evaluationLookUp[(int)move.MovePieceType, move.StartSquare.File + 8 * (evaluateForWhite ? move.StartSquare.Rank : 7 - move.StartSquare.Rank)];

        if (move.IsCapture)
            delta += evaluationLookUp[(int)move.CapturePieceType, move.TargetSquare.File + 8 * (!evaluateForWhite ? move.TargetSquare.Rank : 7 - move.TargetSquare.Rank)];

        if (move.IsPromotion)
            delta += evaluationLookUp[(int)move.PromotionPieceType, move.TargetSquare.File + 8 * relativeTargetSquareRank];
        else
            delta += evaluationLookUp[(int)move.MovePieceType, move.TargetSquare.File + 8 * relativeTargetSquareRank];

        delta *= evaluateForWhite ? 1 : -1;

        return delta;
    }
}