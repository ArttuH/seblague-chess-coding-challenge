﻿//#define LOG
#define NOLOG

using ChessChallenge.API;
using System;

public class BaltimoreBot : IChessBot
{
    const float EVALUATION_PAWN = 1f;
    const float EVALUATION_KNIGHT = 3f;
    const float EVALUATION_BISHOP = 3f;
    const float EVALUATION_ROOK = 5f;
    const float EVALUATION_QUEEN = 9f;
    const float EVALUATION_CHECK = 2f;//100f;
    const float EVALUATION_MATE = 9001f;
    const float EVALUATION_DRAW = -1000f;
    const float EVALUATION_USELESS_MOVE = -1f;

    bool playingAsWhite = false;

    enum Phase
    {
        Opening,
        MiddleGame,
        EndGame
    }

    Phase phase;

    public BaltimoreBot()
    {
        //phase = Phase.Opening;
    }

    public Move Think(Board board, Timer timer)
    {
        playingAsWhite = board.IsWhiteToMove;
        float evaluationBeforeMove = Evaluate(board, true); // Also update game phase
#if LOG
        Console.WriteLine($"Bot playing as {(playingAsWhite ? "white" : "black")}");
        //Console.WriteLine($"Evaluation before our move is {evaluationBeforeMove}");
#endif
        //int depth = timer.MillisecondsRemaining > 1e4 ? 4 : 4;

        Move bestMove = FindBestMove(board, true, 4); // 4

#if LOG
        //Console.WriteLine($"***\nPlaying move {bestMove}\n***\n");
        Console.WriteLine($"***\nPlaying move {MoveToString(bestMove)}");
        float evaluationAfterMove = Evaluate(bestMove, board);
        Console.WriteLine($"The move leads to evaluation {evaluationAfterMove} for a change of {evaluationAfterMove - evaluationBeforeMove}\n***\n\n");
#endif

        return bestMove;
    }

    Move FindBestMove(Board board, bool ourMove, int depth)
    {
#if LOG
        Console.WriteLine($"\nFindBestMove at depth: {depth}\n");
#endif

        Move[] legalMoves = board.GetLegalMoves();

        float evaluationBeforeMove = Evaluate(board);

        float[] evaluations = new float[legalMoves.Length];

        for (int i = 0; i < legalMoves.Length; ++i)
        {
            Move move = legalMoves[i];

            int depthForThisLine = depth;

            if (depthForThisLine > 1)
            {
#if LOG
                Console.WriteLine($"\n\nLooking at {(ourMove ? "our" : "opponent's")} move {MoveToString(move)}");
#endif
                evaluations[i] = Evaluate(move, board);

                if (!EvaluationIsBetter(evaluations[i], evaluationBeforeMove, board.IsWhiteToMove))
                {
                    evaluations[i] += (board.IsWhiteToMove ? 1 : -1) * EVALUATION_USELESS_MOVE; // Penalize the useless move
#if LOG
                    Console.WriteLine($"No need to evaluate any further, as this move does not improve the board state");
#endif
                    // Only look one move ahead to see if the "useless" move was actually to dodge a capture
                    if (depthForThisLine > 2)
                        depthForThisLine = 2;
                }
#if LOG
                Console.WriteLine($"By itself the move leads to evaluation {evaluations[i]} for a change of {evaluations[i] - evaluationBeforeMove}");
#endif

                board.MakeMove(move);

                if (board.IsInCheckmate())
                {
                    board.UndoMove(move);
                    return move;
                }
                else if (board.IsDraw())
                {
                    evaluations[i] = Evaluate(board);
                    board.UndoMove(move);
                }
                else
                {
                    Move bestCounterMove = FindBestMove(board, !ourMove, depthForThisLine - 1);
                    board.MakeMove(bestCounterMove);
                    evaluations[i] = Evaluate(board);
#if LOG
                    Console.WriteLine($"At depth {depth}, against {MoveToString(move)}, the best counter move for {(ourMove ? "opponent" : "us")} is {MoveToString(bestCounterMove)}, leading to evaluation {evaluations[i]}\n");
#endif
                    board.UndoMove(bestCounterMove);
                    //Console.WriteLine($"Best countermove for {(board.IsWhiteToMove ? "white" : "black")} {MoveToString(bestCounterMove)} changes evaluation from {sanityCheck} to {evaluations[i]}");
                    board.UndoMove(move);
                }
            }
            else
            {
#if LOG
                if (ourMove)
                    Console.WriteLine($"\n\nLooking at {(ourMove ? "our" : "opponent's")} move {MoveToString(move)}");
#endif
                evaluations[i] = Evaluate(move, board);
            }

            //Console.WriteLine($"Evaluation after {(board.IsWhiteToMove ? "white" : "black")}'s move {MoveToString(move)} was updated from {evaluationByItself} to {evaluations[i]}");
        }

        Move bestMove = FindBestMove(evaluations, legalMoves, board.IsWhiteToMove);

        return bestMove;
    }

    float Evaluate(Move move, Board board)
    {
#if LOG
        //Console.WriteLine($"Looking at move {MoveToString(move)}");
#endif

        board.MakeMove(move);
        float evaluation = Evaluate(board);

        board.UndoMove(move);

        return evaluation;
    }

    float Evaluate(Board board, bool updatePhase = false)
    {
#if LOG
        //Console.WriteLine($"board.IsWhiteToMove == {board.IsWhiteToMove}");
#endif

        float evaluation = 0f;

        int totalPieces = 0;
        int ownUndevelopedPieces = 0;

        if (board.IsInCheckmate())
            evaluation += EVALUATION_MATE;
        else if (board.IsInCheck())
            evaluation += EVALUATION_CHECK;
        else if (board.IsDraw())
            evaluation += EVALUATION_DRAW;

        evaluation *= (board.IsWhiteToMove ? -1 : 1);

        evaluation += EvaluatePieces(board, true, ref totalPieces, ref ownUndevelopedPieces);
        evaluation += EvaluatePieces(board, false, ref totalPieces, ref ownUndevelopedPieces);

        if (updatePhase && phase == Phase.Opening && ownUndevelopedPieces <= 1)
        {
            phase = Phase.MiddleGame;
#if LOG
#endif
            Console.WriteLine("Transitioning to middlegame!");
        }
        if (updatePhase && phase != Phase.EndGame && totalPieces < 6)
        {
            phase = Phase.EndGame;
#if LOG
#endif
            Console.WriteLine("We're in the endgame now!");
        }

        //Console.WriteLine($"Evaluation: {evaluation}");

        return evaluation;
    }

    float EvaluatePieces(Board board, bool evaluateForWhite, ref int totalPieces, ref int ownUndevelopedPieces)
    {
        float evaluation = 0;

        PieceList pawns = board.GetPieceList(PieceType.Pawn, evaluateForWhite);
        PieceList knights = board.GetPieceList(PieceType.Knight, evaluateForWhite);
        PieceList bishops = board.GetPieceList(PieceType.Bishop, evaluateForWhite);
        PieceList rooks = board.GetPieceList(PieceType.Rook, evaluateForWhite);
        PieceList queens = board.GetPieceList(PieceType.Queen, evaluateForWhite);

        foreach (Piece piece in pawns)
            evaluation += EVALUATION_PAWN + EvaluatePawnSquare(piece.Square, evaluateForWhite);
        foreach (Piece piece in knights)
        {
            if (playingAsWhite == evaluateForWhite && piece.Square.Rank == (evaluateForWhite ? 0 : 7))
                ++ownUndevelopedPieces;
            evaluation += EVALUATION_KNIGHT + EvaluateKnightSquare(piece.Square, evaluateForWhite);
            ++totalPieces;
        }
        foreach (Piece piece in bishops)
        {
            if (playingAsWhite == evaluateForWhite && piece.Square.Rank == (evaluateForWhite ? 0 : 7))
                ++ownUndevelopedPieces;
            evaluation += EVALUATION_BISHOP + EvaluateBishopSquare(piece.Square, evaluateForWhite);
            ++totalPieces;
        }
        foreach (Piece piece in rooks)
        {
            evaluation += EVALUATION_ROOK + EvaluateRookSquare(piece.Square);
            ++totalPieces;
        }
        foreach (Piece piece in queens)
        {
            evaluation += EVALUATION_QUEEN;
            ++totalPieces;
        }

        evaluation += EvaluateKingSquare(board.GetPieceList(PieceType.King, evaluateForWhite)[0].Square,
            board.GetPieceList(PieceType.King, !evaluateForWhite)[0].Square, evaluateForWhite);

        evaluation *= (evaluateForWhite ? 1 : -1);

        return evaluation;
    }

    Move FindBestMove(float[] evaluations, Move[] legalMoves, bool evaluateForWhite)
    {
        Array.Sort(evaluations, legalMoves);

        if (evaluateForWhite)
            return legalMoves[legalMoves.Length - 1];
        else
            return legalMoves[0];

    }

    float EvaluatePawnSquare(Square square, bool white)
    {
        int relativeRank = white ? (square.Rank - 1) : (6 - square.Rank);

        float distanceFromCenter = square.File <= 3 ? (3.5f - square.File) : (square.File - 3.5f);

        float evaluation = (relativeRank) * 0.125f;

        evaluation /= distanceFromCenter;

        return evaluation;
    }

    float EvaluateKnightSquare(Square square, bool white)
    {
        int relativeRank = white ? (square.Rank) : (7 - square.Rank);
        float fileDistanceFromCenter = square.File <= 3 ? (3.5f - square.File) : (square.File - 3.5f);

        float evaluation;
        if (phase == Phase.Opening)
        {
            evaluation = relativeRank <= 1 ? 0.0f : relativeRank < 4 ? 0.6f : 0.0f;
            evaluation /= fileDistanceFromCenter;
        }
        else
        {
            float rankDistanceFromCenter = relativeRank <= 3 ? (3.5f - relativeRank) : (relativeRank - 3.5f);

            evaluation = 1 / (rankDistanceFromCenter + fileDistanceFromCenter);
        }

        return evaluation;
    }

    float EvaluateBishopSquare(Square square, bool white)
    {
        float evaluation = 0;
        int relativeRank = white ? (square.Rank) : (7 - square.Rank);
        if (phase == Phase.Opening)
            evaluation = relativeRank == 0 ? 0.0f : relativeRank < 4 ? 0.35f : 0;

        return evaluation;
    }

    float EvaluateRookSquare(Square square)
    {
        float evaluation = 0;

        if (phase == Phase.Opening)
            evaluation = square.File == 0 ? 0.0f : square.File == 7 ? 0.0f : 0.1f;

        return evaluation;
    }

    float EvaluateKingSquare(Square ownSquare, Square opponentSquare, bool white)
    {
        float evaluation = 0;

        //int relativeRank = white ? (square.Rank) : (7 - square.Rank);

        if (phase == Phase.MiddleGame)
        {
            float fileDistanceFromSafety = ownSquare.File <= 3 ? MathF.Abs(1.1f - ownSquare.File) : MathF.Abs(5.9f - ownSquare.File);
            evaluation = 0.5f / (fileDistanceFromSafety + (white ? (ownSquare.Rank) : (7 - ownSquare.Rank)));
        }
        else if (phase == Phase.EndGame)
        {
            float distanceSquaredFromOpponentKing = (ownSquare.File - opponentSquare.File) * (ownSquare.File - opponentSquare.File) +
                (ownSquare.Rank - opponentSquare.Rank) * (ownSquare.Rank - opponentSquare.Rank);

            evaluation = 1 / distanceSquaredFromOpponentKing;
        }

        return evaluation;
    }

#if LOG
    string MoveToString(Move move)
    {
        return $"{move.MovePieceType} from {move.StartSquare.Name} to {move.TargetSquare.Name}";
    }
#endif

    bool EvaluationIsBetter(float evaluation, float baselineEvaluation, bool evaluateForWhite)
    {
        return evaluateForWhite ? evaluation > baselineEvaluation : evaluation < baselineEvaluation;
    }
}